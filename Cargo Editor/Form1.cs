﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string[] filenames;
        string regex = @"mass[]: [0-9]*";

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog open = new FolderBrowserDialog();
            if (open.ShowDialog() == DialogResult.OK)
                textBox1.Text = open.SelectedPath;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            filenames = new string[Directory.GetFiles(textBox1.Text).Length];
            filenames = Directory.GetFiles(textBox1.Text);
            for (int i = 0; i < filenames.Length; i++)
            {
                using (StreamReader read = new StreamReader(new FileStream(filenames[i], FileMode.Open, FileAccess.Read)))
                {
                    string file_data = read.ReadToEnd();
                    foreach (Match match in Regex.Matches(file_data, regex, RegexOptions.IgnoreCase))
                        listBox1.Items.Add(String.Format("{0} - Вес: {1}", filenames[i], match.Value.Split(' ')[1].Trim()));
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            for (int i = 0; i < filenames.Length; i++)
            {
                using (StreamReader read = new StreamReader(new FileStream(filenames[i], FileMode.Open, FileAccess.Read)))
                {
                    string text_data = read.ReadToEnd();
                    string[] text_lines = text_data.Split('\n');
                    text_data = string.Empty;
                    for (int j = 0; j < text_lines.Length; j++)
                    {
                        if (Regex.IsMatch(text_lines[j], regex))
                            text_lines[j] = String.Format("\tmass[]: {0}.0", textBox2.Text);
                        text_data += text_lines[j] + "\n";
                    }
                    read.Close();
                    using (StreamWriter write = new StreamWriter(new FileStream(filenames[i], FileMode.Create, FileAccess.Write)))
                    {
                        write.Write(text_data);
                    }
                }
            }
            MessageBox.Show("Новый вес успешно записан.", "Cargo Editor for ETS 1.3.1", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }
    }
}
