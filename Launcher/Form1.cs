﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

namespace Launcher
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (File.Exists(Application.StartupPath + "\\bin\\Cargo Editor.exe"))
            {
                FileVersionInfo ver = FileVersionInfo.GetVersionInfo(Application.StartupPath + "\\bin\\Cargo Editor.exe");
                label3.ForeColor = Color.Green;
                label3.Text = ver.FileVersion;
                button1.Enabled = true;
            }
            else
            {
                label3.ForeColor = Color.Red;
                label3.Text = "Неизвестно";
                button1.Enabled = false;
            }
            if (File.Exists(Application.StartupPath + "\\bin\\Weight Editor.exe"))
            {
                FileVersionInfo ver = FileVersionInfo.GetVersionInfo(Application.StartupPath + "\\bin\\Weight Editor.exe");
                label4.ForeColor = Color.Green;
                label4.Text = ver.FileVersion;
                button2.Enabled = true;
            }
            else
            {
                label4.ForeColor = Color.Red;
                label4.Text = "Неизвестно";
                button2.Enabled = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Process.Start(Application.StartupPath + "\\bin\\Cargo Editor.exe");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Process.Start(Application.StartupPath + "\\bin\\Weight Editor.exe");
        }
    }
}
