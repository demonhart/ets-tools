﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace Weight_Editor
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string[] data = new string[5];
        string[] data2 = new string[5];
        string regex = @"([a-z]*\S){1,2}mass:\s*[0-9]*";

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog open = new FolderBrowserDialog();
            if (open.ShowDialog() == DialogResult.OK)
                textBox1.Text = open.SelectedPath;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(textBox1.Text))
            {
                try
                {
                    using (StreamReader read = new StreamReader(new FileStream(textBox1.Text + "\\vehicle\\physics.sii", FileMode.Open, FileAccess.Read)))
                    {
                        string text_data = read.ReadToEnd();
                        int index = 0;
                        foreach (Match match in Regex.Matches(text_data, regex, RegexOptions.IgnoreCase))
                        {
                            data[index] = match.Value.Replace("\t", String.Empty).Split(':')[0].Trim();
                            data2[index] = match.Value.Replace("\t", String.Empty).Split(':')[1].Trim();
                            index++;
                        }
                        textBox2.Text = data2[0];
                        textBox3.Text = data2[1];
                        textBox4.Text = data2[2];
                        textBox5.Text = data2[3];
                        textBox6.Text = data2[4];
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            data2[0] = textBox2.Text;
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            data2[1] = textBox3.Text;
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            data2[2] = textBox4.Text;
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            data2[3] = textBox5.Text;
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            data2[4] = textBox6.Text;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(textBox1.Text))
            {
                try
                {
                    using (StreamReader read = new StreamReader(new FileStream(textBox1.Text + "\\vehicle\\physics.sii", FileMode.Open, FileAccess.Read)))
                    {
                        string text_data = read.ReadToEnd();
                        string[] text_lines = text_data.Split('\n');
                        text_data = String.Empty;
                        int index = 0;
                        for (int i = 0; i < text_lines.Length; i++)
                        {
                            if (Regex.IsMatch(text_lines[i], regex, RegexOptions.IgnoreCase))
                            {
                                text_lines[i] = String.Format("\t{0}:\t\t\t{1}", data[index], data2[index]);
                                index++;
                            }
                            text_data += text_lines[i] + "\n";
                        }
                        read.Close();
                        using (StreamWriter write = new StreamWriter(new FileStream(textBox1.Text + "\\vehicle\\physics.sii", FileMode.Create, FileAccess.Write)))
                        {
                            write.Write(text_data);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
}
